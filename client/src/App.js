import React, { Component, Suspense } from "react";
import { Route } from "react-router-dom";

import ModalSwitch from "./ModalSwitch";

import { LoadingPage } from "./components/pages/LoadingPage";

export class App extends Component {
    static defaultProps = {};

    componentDidMount() {
        if ( window ) {
            document.body.style.opacity = 1;
        }
    }

    render() {
        return (
            <div>
                <Route component={ModalSwitch} />
            </div>
        );
    }
};

export default function AppWrap() {
    return (
        <Suspense fallback={ <LoadingPage/> }>
            <App/>
        </Suspense>
    );
};