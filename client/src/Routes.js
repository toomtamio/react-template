import {
    HomePage
} from "./components/pages";

export default {
    HomePage: {
        path: "/",
        exact: true,
        component: HomePage
    }
};
