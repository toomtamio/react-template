import { combineReducers } from "redux";

import ajax from "./ajaxReducer";

const rootReducer = combineReducers({
    ajax
});

export default rootReducer;
