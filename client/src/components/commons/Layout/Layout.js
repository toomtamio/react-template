import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Div = styled.div`
    width: 100%;
    position: relative;
`;

export const Layout = ({ children }) => {
    return (
        <Div>
            {children}
        </Div>
    )
};

Layout.propTypes = {
};

Layout.defaultProps = {
};

export default Layout;