import React from "react";
import Loadable from "react-loadable";
import { LoadingPage as LPage } from "./LoadingPage";

export { LoadingPage } from "./LoadingPage";

export { HomePage } from "./HomePage";

// export const MessagePage = Loadable( {
//     loader: () => import(/* webpackChunkName: "MessagePageChunk"*/ "./User/MessagePage/MessagePage"),
//     loading: () => <LPage/>,
//     modules: [ "MessagePageChunk" ]
// } );