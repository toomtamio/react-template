import React from "react";
import styled from "styled-components";
// import { Loading } from "components/commons/Loading";

const Div = styled.div`
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    position: fixed;
    background: #fff;

    .loading-wrap {
        top: 50%;
        left: 50%;
        position: absolute;
        transform: translateX(-50%) translateY(-50%);
    }
`;

const LoadingPage = () => {
    return (
        <Div>
            <div className="loading-wrap">
                {/* <Loading /> */}
                Loading...
            </div>
        </Div>
    )
};

export default LoadingPage;