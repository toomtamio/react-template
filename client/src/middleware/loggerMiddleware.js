import config from "config";
export default ({ getState }) => next => action => {
    if (config.env !== "prod") {
        console.group(action.type);
        console.info('payload:', action.payload);
        next(action);
        console.log('state:', getState());
        console.groupEnd();
    } else {
        next(action);
    }
};