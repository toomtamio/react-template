import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";

import Routes from "./Routes";

import Layout from "./components/commons/Layout/Layout";
import { isBrowser } from "utils/function";

const Div = styled.div`
    width: 100%;
    position: relative;
`;

export class ModalSwitch extends Component {
    previousLocation = this.props.location;

    constructor( props ) {
        super( props );

        this.state = {
            width: 0,
            height: 0,
            top: 0
        };

        this.onCloseModal = this.onCloseModal.bind( this );
        this.updateDimensions = this.updateDimensions.bind( this );
    }

    componentWillMount() {
        this.updateDimensions();
    }

    componentDidMount() {
        if ( isBrowser() ) {
            this.updateDimensions();
            window.addEventListener( "resize", this.updateDimensions );
        }
    }

    componentDidUpdate( prevProps, prevState ) {
        if ( isBrowser() ) {
            if ( `${ prevState.width }` === "0" && `${ prevState.height }` === "0" ) {
                this.updateDimensions();
            }
        }
    }

    componentWillUnmount() {
        if ( isBrowser() ) {
            window.removeEventListener( "resize", this.updateDimensions );
        }
    }

    componentWillUpdate( nextProps ) {
        let { location } = this.props;
        if ( nextProps.history.action !== "POP" && ( !location.state || !location.state.modal ) ) {
            this.previousLocation = this.props.location;
        }
    }

    updateDimensions() {
        if ( isBrowser() ) {
            this.setState( {
                width: document.body.clientWidth || window.innerWidth,
                height: document.body.clientHeight || window.innerHeight
            }, () => {
                //
            } );
        }
    }

    onCloseModal() {
        this.props.history.push( this.previousLocation );
    }

    render() {
        let { location } = this.props;

        let isModal = !!(
            location.state &&
            location.state.modal &&
            this.previousLocation !== location &&
            this.previousLocation.pathname !== location.pathname
        );

        return (
            <Div>
                <Layout>
                    <Switch location={ isModal ? this.previousLocation : location }>
                        { Object.keys( Routes ).map( routeKey => (
                            <Route key={ routeKey } { ...Routes[ routeKey ] }/>
                        ) ) }
                    </Switch>
                </Layout>
            </Div>
        );
    }
};

const mapStateToProps = ( state, ownProps ) => {
    return {
        loading: state.ajax > 0,
    };
};

const mapDispatchToProps = ( dispatch ) => {
    return {
    };
};

export default connect( mapStateToProps, mapDispatchToProps )( ModalSwitch );
