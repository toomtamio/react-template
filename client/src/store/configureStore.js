import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "../reducers";

export default ({ initialState, middlewares = [], enhancers = [] }) => {
	const middlewareEnhancer = applyMiddleware(...middlewares);
	const composedEnhancers = compose(middlewareEnhancer, ...enhancers);

	const store = createStore(rootReducer, initialState, composedEnhancers);

	return store;
};
