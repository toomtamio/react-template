module.exports = {
    port: 3001,
    env: "local",
    worker: false,
    database: {
        name: "toomtam",
        username: "root",
        password: "root",
        host: "localhost",
        driver: "mysql",
    },
    mongo: {
        host: "localhost",
        name: "toomtam",
        port: "27017",
        query: "",
        username: "",
        password: ""
    },
    auth: {
        secretKey: "E301EFFEAA1C1D4FE9B8EEA51DAF0E36!30o33@m",
		token: "toomtam.io"
    },
    origin: `http://localhost:3001`,
    email: {
        username: "songkrod@toomtam.io",
		password: "Mujiyd_3d"
    },
    path: {
        tmp: "/uploads/tmp",
        thumbs: "/uploads/thumbs",
        image: "/uploads/images",
        video: "/uploads/videos",
    },
};