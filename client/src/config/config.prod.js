module.exports = {
	port: 3000,
	env: "prod",
	worker: true,
	database: {
		name: "toomtam",
		username: "toomtam",
		password: "!30o33@m",
		host: "localhost",
		driver: "mysql"
	},
	mongo: {
		host: "localhost",
		name: "toomtam",
		port: "27017",
		query: "",
		username: "",
        password: ""
	},
	auth: {
		secretKey: "E301EFFEAA1C1D4FE9B8EEA51DAF0E36!30o33@m",
		token: "toomtam.io"
	},
    origin: "https://toomtam.io",
	email: {
		username: "songkrod@toomtam.io",
		password: "Mujiyd_3d"
	},
	path: {
		tmp: "/uploads/tmp",
		thumbs: "/uploads/thumbs",
		image: "/uploads/images",
		video: "/uploads/videos"
	}
};