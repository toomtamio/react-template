import axios from "axios";
import config from "../config";
import { Token } from "../utils";

export const get = (url = "", params = {}, { headers = {} } = {}) => {
	return axios.get(`${config.origin}${url}`, {
		headers: {
			Authorization: `Bearer ${Token.getToken()}`,
			...headers,
		},
        params: params,
        data: params
	});
};

export const post = (url = "", params = {}, { headers = {} } = {}) => {
	return axios.post(`${config.origin}${url}`, params, {
		headers: {
			Authorization: `Bearer ${Token.getToken()}`,
			...headers,
		}
	});
};
