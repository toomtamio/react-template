import React from "react";
import { hydrate } from "react-dom";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { BrowserRouter } from "react-router-dom";
import configureStore from "./store/configureStore";
import { loggerMiddleWare } from "./middleware";

import "./icons.css";
import "./reset.css";
import "./index.css";
import App from "./App";

import rawInitialState from "./reducers/initialState";

import * as serviceWorker from "./serviceWorker";

import "./i18n";

const initialState = window.__INITIAL_STATE__ || rawInitialState;

delete window.__INITIAL_STATE__;

const store = configureStore( {
    initialState,
    middlewares: [
        thunk,
        loggerMiddleWare
    ]
} );

hydrate(
    <Provider store={ store }>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>,
    document.getElementById( "root" )
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
