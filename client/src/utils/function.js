import moment from "moment";
import config from "../config";

export const weekdays = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
];

export const months = [
    "มกราคม",
    "กุมภาพันธ์",
    "มีนาคม",
    "เมษายน",
    "พฤษภาคม",
    "มิถุนายน",
    "กรกฎาคม",
    "สิงหาคม",
    "กันยายน",
    "ตุลาคม",
    "พฤศจิกายน",
    "ธันวาคม"
];

export const shortMonths = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
];

export const weekdaysData = () => {
    const result = [];
    for ( let i = 1; i <= 7; i++ ) {
        result.push( { id: i.toString().padStart( 2, "0" ), label: weekdays[ i - 1 ] } );
    }
    return result;
};

export const dateData = () => {
    const result = [];
    for ( let i = 1; i <= 31; i++ ) {
        result.push( { id: i.toString().padStart( 2, "0" ), label: `${ i }` } );
    }
    return result;
};

export const monthData = () => {
    const result = [];
    for ( let i = 1; i <= 12; i++ ) {
        result.push( { id: i.toString().padStart( 2, "0" ), label: shortMonths[ i - 1 ] } );
    }
    return result;
};

export const yearData = () => {
    const result = [];
    const start = ( new Date().getFullYear() ) - 10;
    const end = start - 120;
    for ( let i = start; i > end; i-- ) {
        result.push( { id: i.toString().padStart( 4, "0" ), label: `${ i }` } );
    }
    return result;
};

export const formatDate = dateStr => {
    let date = new Date( dateStr );
    return `${ strPad( date.getDate().toString() ) } ${ months[ date.getMonth() ] } ${ parseInt( date.getFullYear() ) + 543 }`;
};

export const formatDateRange = ( fromDateStr, toDateStr ) => {
    const fromDate = new Date( fromDateStr );
    const fromYear = fromDate.getFullYear();
    const fromMonth = fromDate.getMonth();
    const fromDay = fromDate.getDate();
    const toDate = new Date( toDateStr );
    const toYear = toDate.getFullYear();
    const toMonth = toDate.getMonth();
    const toDay = toDate.getDate();

    if ( fromYear !== toYear ) return `${ fromDay } ${ shortMonths[ fromMonth ] } ${ fromYear } - ${ toDay } ${ shortMonths[ toMonth ] } ${ toYear }`;
    else if ( fromMonth !== toMonth ) return `${ fromDay } ${ shortMonths[ fromMonth ] } - ${ toDay } ${ shortMonths[ toMonth ] } ${ toYear }`;
    else if ( fromDay !== toDay ) return `${ fromDay } - ${ toDay } ${ shortMonths[ toMonth ] } ${ toYear }`;
    else return `${ toDay } ${ shortMonths[ toMonth ] } ${ toYear }`;
};

export const strPad = ( number = 0, length = 2 ) => {
    return ( "0000000000" + number ).slice( -length );
};

export const getAssets = path => {
    return `/assets/images/${ path }`;
};

export const postsFormatter = posts => {
    return posts.map( i => {
        return {
            id: i.id,
            title: `${ i.auther.firstName } ${ i.auther.lastName }`,
            msg: i.msg,
            raw: { ...i }
        };
    } );
};

export function generateTimeArray( { minutes = [ 0 ], seconds = [ 0 ] } ) {
    const timeArray = [];
    const mins = minutes.map( ( x ) => {
        if ( x < 0 || x > 59 ) throw new Error( "invalid minutes parameter" );
        return strPad( x, 2 );
    } );
    const secs = seconds.map( ( x ) => {
        if ( x < 0 || x > 59 ) throw new Error( "invalid seconds parameter" );
        return strPad( x, 2 );
    } );
    for ( let i = 0; i < 24; i++ ) {
        const h = strPad( i, 2 );
        mins.forEach( ( m ) => {
            secs.forEach( ( s ) => {
                timeArray.push( `${ h }:${ m }:${ s }` );
            } );
        } );
    }
    return timeArray;
}

export const numberFormatter = ( n, c = 0, d = ".", t = "," ) => {
    let s = n < 0 ? "-" : "",
        i = String( parseInt( n = Math.abs( Number( n ) || 0 ).toFixed( c ) ) ),
        j = ( j = i.length ) > 3 ? j % 3 : 0;
    return s + ( j ? i.substr( 0, j ) + t : "" ) + i.substr( j ).replace( /(\d{3})(?=\d)/g, "$1" + t ) + ( c ? d + Math.abs( n - i ).toFixed( c ).slice( 2 ) : "" );
};

export const isChecked = ( data, item, key ) => {
    return data.filter( i => i[ key ] === item[ key ] ).length === 1;
};

export const sortDataByValue = ( data, key, dataSort, sortKey ) => {
    if ( data )
        return data[ key ] === "desc" ? dataSort.sort( ( sMin, sMax ) => ( sMin[ sortKey ] - sMax[ sortKey ] ) ) : dataSort.sort( ( sMin, sMax ) => ( sMax[ sortKey ] - sMin[ sortKey ] ) );
    else
        return dataSort;
};

export const formatDateTime = ( date ) => {
    let stillUtc = moment.utc( date ).toDate();
    let formatedDate = moment( stillUtc ).local().fromNow();
    return formatedDate;
};

export const formatDisplayDate = ( datetime ) => {
    let stillUtc = moment.utc( datetime ).toDate();
    let formatedDate = moment( stillUtc ).local();
    return {
        date: formatedDate.format( "DD MMM YYYY" ),
        time: formatedDate.format( "kk:mm:ss" )
    };
};

export const toHHMMSS = ( secs ) => {
    let sec_num = parseInt( secs, 10 );
    let hours = Math.floor( sec_num / 3600 ) % 24;
    let minutes = Math.floor( sec_num / 60 ) % 60;
    let seconds = sec_num % 60;
    return [ hours, minutes, seconds ]
        .map( v => v < 10 ? "0" + v : v )
        .filter( ( v, i ) => v !== "00" || i > 0 )
        .join( ":" );
};

export const randomString = ( length = 10 ) => {
    let text = "";
    let possible =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for ( let i = 0; i < length; i++ )
        text += possible.charAt( Math.floor( Math.random() * possible.length ) );

    return text;
};

export const timestamp = () => {
    return Math.floor( new Date() / 1000 );
};

export const getUniqueString = ( separate = "_", length = 10 ) => {
    return `${ timestamp() }${ separate }${ randomString( length ) }`;
};

export const getRoundUpDate = ( { nextHours = 1 } = {} ) => {
    const date = new Date();
    date.setMilliseconds( 0 );
    date.setSeconds( 0 );
    date.setMinutes( 0 );
    date.setHours( date.getHours() + nextHours );
    return date;
};

export const getDateOnly = ( date ) => {
    const result = new Date( date );
    result.setMilliseconds( 0 );
    result.setSeconds( 0 );
    result.setMinutes( 0 );
    result.setHours( 0 );
    return result;
};

export const getDateOnlyStr = ( date ) => {
    return date.toJSON().slice( 0, 10 );
};

export const upsertOneArrayOfObject = ( prevArray, newItem, { key = "id" } = {} ) => {
    const result = [ ...prevArray ];
    const index = result.findIndex( x => x[ key ] === newItem[ key ] );
    if ( index > -1 ) result[ index ] = newItem;
    else result.push( newItem );

    return result;
};

export const upsertManyArrayOfObject = ( prevArray, newArray, { key = "id" } = {} ) => {
    const result = [ ...prevArray ];
    newArray.forEach( item => {
        const index = result.findIndex( x => x[ key ] === item[ key ] );
        if ( index > -1 ) result[ index ] = item;
        else result.push( item );
    } );

    return result;
};

export const deleteOneArrayOfObject = ( prevArray, id, { key = "id" } = {} ) => {
    const result = [ ...prevArray ];
    const index = result.findIndex( x => x[ key ] === id );
    if ( index > -1 ) result.splice( index, 1 );

    return result;
};

export const isSupportWebP = () => {
    if ( document && localStorage ) {
        return localStorage.getItem( "webp" ) ? true : false;
    } else {
        return false;
    }
};

export const getPositiveWord = () => {
    const positiveWord = [ "Woo! Hoo!", "Hooray!", "Yeah!", "Yahoo!", "Yay!", "Success!" ];
    const k = Math.floor( Math.ceil( Math.random() * positiveWord.length * 10 ) / 10 );
    return positiveWord[ k ];
};

export const debug = ( ...args ) => {
    if ( config.env !== "prod" ) {
        console.log( "DEBUG:", ...args );
    }
};

export const getProfileImage = user => {
    return user ? user.img : "/assets/images/icon-guest.svg";
};

export const getImageUrl = image => {
    return image ? image.url : "";
};

export const genUniqueChatRoomKey = ( { userIds = [] } ) => {
    return userIds.sort( ( a, b ) => parseInt( a ) - parseInt( b ) ).join( "_" );
};

export const isBrowser = () => {
    return global[ "window" ] ? true : false;
};

export const getOrigin = () => {
    return isBrowser() ? global[ "window" ].location.origin : "";
};

export const escapeRegExp = ( text ) => {
    return text.replace( /[.*+?^${}()|[\]\\]/g, "\\$&" );
};

export const arraysEqual = ( a, b ) => {
    if ( a === b ) return true;
    if ( a == null || b == null ) return false;
    if ( a.length !== b.length ) return false;
    for ( let i = 0; i < a.length; ++i ) {
        if ( a[ i ] !== b[ i ] ) return false;
    }
    return true;
};
