import config from "../config";
import { isBrowser } from "./function";

export const getToken = () => {
    return isBrowser() ? localStorage.getItem(config.auth.token) : null;
};

export const setToken = token => {
    if (isBrowser()) {
        localStorage.setItem(config.auth.token, token);
    }

    setCookie(config.auth.token, token);
}
export const clearToken = () => {
    if (isBrowser()) {
        localStorage.removeItem(config.auth.token);
    }
    deleteCookie(config.auth.token);
    deleteCookie("access-token");
    deleteCookie("refresh-token");
}

export const getUser = () => {
    return isBrowser() ? localStorage.getItem("user") : null;
};

export const setUser = user => {
    if (isBrowser()) {
        localStorage.setItem("user", JSON.stringify(user));
    }
}
export const clearUser = () => {
    if (isBrowser()) {
        localStorage.removeItem("user");
    }
}

const setCookie = (cname, cvalue, exdays=7) => {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    const expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

const getCookie = cname => {
    const name = cname + "=";
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

const deleteCookie = cname => {
    setCookie(cname, "", -1);
};