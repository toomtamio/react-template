import socketIOClient from "socket.io-client";
import { getUniqueString } from "./function";
import config from "../config";

let user = {
    id: getUniqueString("+", "20"),
    socket: null
};

export const create = ({ userId="" }) => {
    if (user.socket) {
        return get();
    } else {
        user.socket = socketIOClient(config.origin, {
            upgrade: false,
            transports: ['websocket'],
            query: {
                user: userId,
                private: user.id
            }
        });

        return get();
    }
};

export const get = () => user.socket;