const fs = require("fs");

let data = '';
if (process.env.NODE_ENV === "production") {
	data = 'module.exports = require("./config.prod");';
} else {
    data = 'module.exports = require("./config.local");';
}

const pathToEntry = "./client/src/config/";
fs.writeFileSync(pathToEntry + "index.js", data);