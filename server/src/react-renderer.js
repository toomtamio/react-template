import fs from "fs";
import path from "path";

import React from "../../client/node_modules/react";
import { renderToString } from "../../client/node_modules/react-dom/server";
import { ServerStyleSheet, StyleSheetManager } from "styled-components";
import { Provider } from "react-redux";
import { matchPath, StaticRouter } from "react-router";
import Loadable from "react-loadable";

import getDatas from "./ssr";

import { BUILD_DIR } from "./paths";
import { App } from "../../client/lib/App";
import Routes from "../../client/lib/Routes";
import { default as configureStore } from "../../client/lib/store/configureStore";
import { default as initialState } from "../../client/lib/reducers/initialState";
import { getReactRouterParams } from "./utils";

export default (req, res, next) => {
    const shell = typeof req.query.shell !== 'undefined';
    let match = Object.keys(Routes).filter(routeKey => {
        return matchPath(req.path, {
            path: Routes[routeKey].path,
            exact: Routes[routeKey].exact ? true : undefined
        });
    });

    if (match.length === 0) {
        next();
        return;
    }

    getDatas(req, match[0], shell).then(data => {
        const location = req.url;
        const context = {};

        const store = configureStore({
            initialState: {
                ...initialState,
                ...data.states
            }
        });

        const sheet = new ServerStyleSheet();
        const modules = [];

        const app = renderToString(
            <Provider store={store}>
                <StaticRouter location={location} context={context}>
                    <StyleSheetManager sheet={sheet.instance}>
                        <Loadable.Capture report={moduleName => modules.push(moduleName)}>
                            <App />
                        </Loadable.Capture>
                    </StyleSheetManager>
                </StaticRouter>
            </Provider>
        );

        console.log("Loadable Modules: ", modules);

        const html = fs
            .readFileSync(path.join(BUILD_DIR, "index.html"), "utf8")
            .replace("__ROOT__", app)
            .replace("window.__INITIAL_STATE__=null", `window.__INITIAL_STATE__=${JSON.stringify(store.getState())}`)
            .replace(`<script>window.__SEO__=""</script>`, getSeoTags(data.seo));

        Loadable.preloadAll();
        res.send(html);
    }).catch(error => {
        console.log("Error: ", error);
        next();
        return;
    });
};

const getSeoTags = ({ title="PetPaw", subject="PetPaw", author="PetPaw", description="Best friends come in all breeds.", image="", type="website", origin="", url="", lang="en_US"  }) => {
    let seo = [
        <title>{title}</title>,
        <meta name="referrer" content="always" />,
        <meta name="title" content={title} />,
        <meta name="subject" content={subject} />,
        <meta name="description" content={description.substring(0, 200)} />,

        <meta name="hostname" content={origin} />,

        <meta property="og:type" content={type} />,
        <meta property="og:url" content={url} />,
        <meta property="og:title" content={title} />,
        <meta property="og:description" content={description.substring(0, 200)} />,
        <meta property="og:image" content={image} />,
        <meta property="og:locale" content={lang} />,
        <meta property="article:author" content={author} />,

        <meta property="twitter:card" content="summary_large_image" />,
        <meta property="twitter:url" content={url} />,
        <meta property="twitter:title" content={title} />,
        <meta property="twitter:description" content={description.substring(0, 200)} />,
        <meta property="twitter:image" content={image} />,

        <meta name="og:site_name" content="PetPaw" />,
        <meta name="twitter:site" content="@PetPaw" />,

        <link rel="author" href={author} />,
        <link rel="publisher" href={author} />,
        <meta itemprop="name" content={title} />,
        <meta itemprop="description" content={description.substring(0, 200)} />,
        <meta itemprop="image" content={image} />,

        <link rel="dns-prefetch" href={`//${origin}/`} />,
        <link rel="preconnect" href={`https://${origin}/`} />,
        <link rel="prefetch" href={`https://${origin}/`} />,
        <link rel="prerender" href={`https://${origin}/`} />
    ];

    if (type === "article") {
        seo = [
            ...seo,
            <meta property="op:markup_version" content="v1.0" />,
            <link rel="canonical" href={url} />
        ];
    }

    return renderToString(
        seo
    );
};
