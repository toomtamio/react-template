import api from "./api";
import socket from "./socket.io";

export const ApiRoute = api;
export const SocketRoute = socket;

export default {
    ApiRoute,
    SocketRoute
};