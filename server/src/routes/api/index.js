import express from "express";
import TemplateApi from "./template.route";

export default () => {
    const router = express.Router();

    router.use( "/template", TemplateApi() );

    return router;
};
