import socketIo from "socket.io";
import mongoAdapter from "socket.io-adapter-mongo";

export const SocketIo = (server) => {
    const io = socketIo(server);
    // io.adapter(mongoAdapter('mongodb://' + config.mongo.host + ':' + config.mongo.port + '/adapter?' + config.mongo.query, {
    //     key: 'socketio_adapter'
    // }));

    // io.on('connection', (socket) => {
    //     const query = socket.handshake.query;
    //     UsersSockets.addUser(query.user, query.private, socket);
    //     console.log(`Socket: userId ${query.user} got new connection.`);
    //     console.log(`Socket: ${Object.keys(UsersSockets.getUser(query.user)).length} connected.`);

    //     socket.on("disconnect", () => {
    //         UsersSockets.removeUser(query.user, query.private);
    //         console.log(`Socket: userId ${query.user} disconnected.`);
    //         console.log(`Socket: ${Object.keys(UsersSockets.getUser(query.user)).length} connected.`);
    //     });

    //     socket.on("chat", event => {
    //         onReceiveMessage({
    //             roomId: event.roomId,
    //             senderId: event.senderId,
    //             message: event.message
    //         });
    //     });
    // });
};

export const SocketModel = {
    // UsersSockets,
    // NotificationsSockets
};

export default SocketIo;