import Routes from "../../../client/lib/Routes";
import { getReactRouterParams } from "../utils/common";

const getURL = (origin) => {
    return `https://${origin}`;
};

export default (req, routeKey, isShell=false) => {
    let seo = {
        origin: req.headers.host,
        url: `${getURL(req.headers.host)}${req.path}`,
        image: `${getURL(req.headers.host)}/images/petpaw.jpg`
    };

    return new Promise((resolve, reject) => {
        resolve({
            seo: seo,
            state: {}
        });
    });
};