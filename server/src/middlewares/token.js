import config from "../../../client/src/config";
import jwt from "jsonwebtoken";
import { errorResponse } from "../utils";

export const generateToken = (payload, expiresIn=false) => {
	return new Promise((resolve, reject) => {
		const opts = {}; // expiresIn ? { expiresIn : expiresIn } : {};
		jwt.sign(payload, config.auth.secretKey, {
			...opts
		}, (error, token) => {
			if (error) {
				reject(error);
			} else {
				resolve(token);
			}
		});
	});
};

export const generateAccessToken = payload => {
	return new Promise((resolve, reject) => {
		generateToken(payload, "15 min").then(token => {
			resolve(token);
		}).catch(error => {
			reject(error);
		});
	});
};

export const generateRefreshToken = payload => {
	return new Promise((resolve, reject) => {
		generateToken(payload, "7 days").then(token => {
			resolve(token);
		}).catch(error => {
			reject(error);
		});
	});
};

export const verifyToken = (req, res, next) => {
	let accessToken = false;
	if (res.cookies) {
		accessToken = (res.cookies["access-token"]) ? res.cookies["access-token"] : false;
	}

	const bearerHeader = req.headers.authorization;
	if (bearerHeader) {
		const bearer = bearerHeader.split(" ");
		const bearerToken = bearer[1];
		req.token = bearerToken;
		accessToken = bearerToken;
	}

	if (accessToken) {
		decodeToken(accessToken).then(authInfo => {
			res.locals.authInfo = authInfo;
			next();
		});
	} else {
		res.locals.authInfo = null;
		next();
		// res.status(401).json(errorResponse("Unauthorized."));
	}
};

export const decodeToken = token => {
	return new Promise((resolve, reject) => {
		jwt.verify(token, config.auth.secretKey, (error, authInfo) => {
			if (error) {
				resolve(null);
			} else {
				resolve(authInfo);
			}
		});
	});
};