import Sequelize from "sequelize";
import config from "../../../../client/src/config";
import getSchemas from "./schemas";
import setRelations from "./relations";

export const sequelize = new Sequelize(
	config.database.name,
	config.database.username,
	config.database.password,
	{
		host: config.database.host,
		dialect: config.database.driver,
		define: {
			paranoid: true,
			charset : 'utf8mb4'
		}
	}
);

export const Op = sequelize.Op;

export const Schemas = getSchemas(sequelize);
setRelations();