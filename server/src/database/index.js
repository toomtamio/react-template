import sql from "./mysql";
import mongo from "./mongodb";

export const MySQL = sql;
export const MongoDB = mongo;

export default {
    MySQL,
    MongoDB
};