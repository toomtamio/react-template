import { middleware as c } from "apicache";

const onlyStatus200 = (req, res) => res.statusCode === 200;
export const cache = (times="1 hour") => c(times, onlyStatus200);