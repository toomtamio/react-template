import nodemailer from "nodemailer";
import Mailgun from "mailgun-js";
import config from "../../../client/src/config";

const mailgunOptions = {
    apiKey: "006636e1fef2115473f3f50f7b300d75-713d4f73-681b537a",
    domain: "mg.toomtam.io"
};

export const sendMail = ({
    from = "Support <noreply@toomtam.io>",
    to = [],
    subject = "",
    html="",
    cc = [],
    bcc = []
}) => {
    return new Promise((resolve, reject) => {
        const mailgun = new Mailgun(mailgunOptions);

        let tos = [...to];
        let bccs = [...bcc];
        if (tos.length < 1) {
            if (bccs.length > 0) {
                tos = bccs.slice(0, 1);
                bccs = bccs.slice(1);
            }
        }

        let data = {
            from: from,
            to: tos.join(", "),
            subject: subject,
            html: html
        };

        // data = cc.length > 0 ? { ...data, cc: cc.join( ", " ) } : data;
        data = bccs.length > 0 ? { ...data, bcc: bccs.join( ", " ) } : data;

        mailgun.messages().send(data, (error, body) => {
            if (error) {
                console.log( "Send Mail Error: ", error );
                reject( error );
            } else {
                console.log( "Send Mail Success: ", body );
                resolve(body);
            }
        });
    });
};