import axios from "axios";

export const get = (url = "", params = {}, { headers = {} } = {}) => {
	return axios.get(`${url}`, {
		headers: {
			...headers,
		},
        params: params,
        data: params
	});
};

export const post = (url = "", params = {}, { headers = {} } = {}) => {
	return axios.post(`${url}`, params, {
		headers: {
			...headers,
		}
	});
};
