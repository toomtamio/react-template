export * from "./cache";
export * from "./common";
export * from "./email";
export * from "./file";
import * as _slack from "./notify";

export const Slack = _slack;

export default {
    Slack
}