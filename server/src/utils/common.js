export const randomString = (length = 10) => {
	let text = "";
	let possible =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (let i = 0; i < length; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}

	return text;
};

export const timestamp = () => {
	return Math.floor(new Date() / 1000);
};

export const getUniqueString = (separate = "_", length = 10) => {
	return `${timestamp()}${separate}${randomString(length)}`;
};

export const toPlain = obj => JSON.parse(JSON.stringify(obj));

export const paddy = (num, padlen, padchar) => {
    var pad_char = typeof padchar !== 'undefined' ? padchar : '0';
	var pad = new Array(1 + padlen).join(pad_char);
    return (pad + num).slice(-pad.length);
}

export const formatDate = date => {
	let currentDate = date ? new Date(date) : new Date();
	return {
		date: `${currentDate.getFullYear()}-${paddy(parseInt(currentDate.getMonth()) + 1, 2)}-${paddy(currentDate.getDate(), 2)}`,
		time: `${paddy(currentDate.getHours(), 2)}:${paddy(currentDate.getMinutes(), 2)}:${paddy(currentDate.getSeconds(), 2)}`
	};
};

export const successResponse = (payload={}) => {
	return {
		success: true,
		response: payload
	};
};

export const errorResponse = (message="") => {
	return {
		success: false,
		error: message
	};
};

export const capitalize = (string="") => {
	const text = `${string}`;
	if (text.length > 0) {
		return `${text.charAt(0).toUpperCase()}${text.slice(1)}`;
	} else {
		return string;
	}
}

export const toArray = value => {
	if (value) {
		if (typeof value === 'object' && value.constructor === Array) {
			return [...value];
		} else {
			return [value];
		}
	} else {
		return [];
	}
};

export const getReactRouterParams = (url, route) => {
	const urls = url.split("/");
	const routes = route.split("/");
	let params = {};
	routes.map((pathname, index) => {
		const pathnames = `${pathname}`.split(":");
		if (pathnames.length > 1) {
			params[pathnames[1]] = urls[index];
		}
	});

	return params;
};

export const isNumeric = (value) => {
  return !isNaN(parseFloat(value)) && isFinite(value)
}

export const clamp = (value, min, max) => {
	return Math.min(Math.max(value, min), max)
}
