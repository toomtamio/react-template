import slackNotify from "slack-notify";
import config from "../../../client/lib/config";
const slack = slackNotify(`https://petpawworkspace.slack.com/services/hooks/incoming-webhook?token=xoxp-592596287684-592596288100-684725925409-48ba0574bf765416ee7947c47f94562e`);

export const send = (msg) => {
    slack.send({
        channel: '#error-log',
        icon_url: 'https://petpaw.com/assets/images/icon-logo.svg',
        text: `${msg}`,
        unfurl_links: 1,
        username: `ENV: ${config.env}`
    });
};