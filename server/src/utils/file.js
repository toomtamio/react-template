import fs from "fs";
import xlsx from "node-xlsx";
import formidable from "formidable";
import ffmpeg from "fluent-ffmpeg";
import sharp from "sharp";
import config from "../../../client/src/config";
import fsex from 'fs-extra';

export const getMediaUrl = (media, type, isThumb=false) => {
	if (isThumb) {
		if (type === "video") {
			const names = media.split(".");
			return getUploadThumb(`${names[0]}.jpg`);
		} else {
			return getUploadThumb(media);
		}
	} else {
		if (type === "image") {
			return getUploadImage(media);
		} else if (type === "video") {
			return getUploadVideo(media);
		}
	}
};

export const getUploadThumb = image => {
	return `${config.origin}${config.path.thumbs}/${image}`;
};

export const getUploadImage = image => {
	return `${config.origin}${config.path.image}/${image}`;
};

export const getUploadVideo = video => {
	return `${config.origin}${config.path.video}/${video}`;
};

export const readExcel = ({ path }) => {
	let dataObject = xlsx.parse(`${path}`);
	return dataObject;
};

export const parseUploadFile = (req) => {
	return new Promise((resolve, reject) => {
		let form = new formidable.IncomingForm({ multiples: true });
		form.parse(req, (error, fields, files) => {
			if (error) {
				reject(error);
			} else {
				resolve({fields, files});
			}
		});
	});
};

export const saveUploadFile = (from, to) => {
	let destinationPath = `./public/uploads/${to}`;
	return new Promise((resolve, reject) => {
		fsex.move(from, destinationPath, error => {
			if (error) {
				console.log("Parse Error: ", error);
				reject(error);
			} else {
				resolve(destinationPath);
			}
		});
	});
};

export const cloneUploadFile = (from, to) => {
	let destinationPath = `./public/uploads/${to}`;
	return new Promise((resolve, reject) => {
		fs.copyFile(from, destinationPath, error => {
			if (error) {
				console.log("Parse Error: ", error);
				reject(error);
			} else {
				resolve(destinationPath);
			}
		});
	});
};

export const parseDataJsonObject = fields => {
	let data = fields.data ? fields.data : fields;
	return (typeof data === 'string' || data instanceof String) ? JSON.parse(data) : data;
}

export const videoToMp4 = ({ name="", ext="" }) => {
	return new Promise((resolve, reject) => {
		const command = ffmpeg(`./public${config.path.tmp}/${name}.${ext}`)
			// .audioCodec('libfaac')
			// .videoCodec('libx264')
			.format('mp4')
			.on('progress', (progress) => {
				console.log(`[ffmpeg] ${JSON.stringify(progress)}`);
			})
			.on('error', (err) => {
				console.log(`[ffmpeg] error: ${err.message}`);
				reject(err);
			})
			.on('end', () => {
				console.log('[ffmpeg] finished');
				fs.unlink(`./public${config.path.tmp}/${name}.${ext}`, error => {
					if (error) {
						console.log("Error: ", error);
					}

					resolve();
				});
			})
			.save(`./public${config.path.video}/${name}.mp4`);
	});
}

export const createVideoThumbnail = ({ name="", ext="" }) => {
	return new Promise((resolve, reject) => {
		const proc = ffmpeg(`./public${config.path.video}/${name}.${ext}`)
			.on('end', function() {
				console.log('[ffmpeg] screenshots were saved');
				resizeImage({
					raw: sharp(`./public${config.path.tmp}/${name}.jpg`),
					output: `./public${config.path.thumbs}/${name}.jpg`,
					option: {
						width: 400,
						height: 400
					}
				}).then(() => {
					resolve();
				}).catch(error => {
					console.log("Error: ", error);
					reject(error);
				});
			})
			.on('error', function(err) {
				console.log(`[ffmpeg] error: ${err.message}`);
				reject(err);
			})
			.takeScreenshots({
				count: 1,
				timemarks: [ '00:00:01.000' ],
				folder: `./public${config.path.tmp}/`,
				filename: `${name}.jpg`
			});
	});
};

const resizeImage = ({ raw, output="", option={} }) => {
	return new Promise((resolve, reject) => {
		raw.resize({
			...option
		})
		.jpeg()
		.toFile(output)
		.then(() => {
			resolve();
		}).catch(error => {
			reject(error);
		});
	});
}

export const createImageThumb = ({ name="", ext="" }) => {
	return new Promise((resolve, reject) => {
		const raw = sharp(`./public${config.path.tmp}/${name}.${ext}`);
		raw.metadata().then(meta => {
			const queries = [
				new Promise((resolve, reject) => {
					resizeImage({
							raw: raw,
							output: `./public${config.path.image}/${name}.jpg`,
							option: {
								width: meta.width >= 1000 ? 1000 : meta.width
							}
						}).then(() => {
							resolve();
						}).catch(error => {
							console.log("Error: ", error);
							reject(error);
						});
				}),
				new Promise((resolve, reject) => {
					resizeImage({
						raw: raw,
						output: `./public${config.path.thumbs}/${name}.jpg`,
						option: {
							width: 400,
							height: 400
						}
					}).then(() => {
						resolve();
					}).catch(error => {
						console.log("Error: ", error);
						reject(error);
					});
				})
			];

			Promise.all(queries).then(() => {
				fs.unlink(`./public${config.path.tmp}/${name}.${ext}`, error => {
					if (error) {
						console.log("Error: ", error);
					}

					resolve();
				});
			}).catch(error => {
				console.log("Error: ", error);
				reject(error);
			})
		}).catch(error => {
			console.log("Error: ", error);
			reject(error);
		});
	});
};